# Docstring:
"""
Speicalisation of the ALIGN_ARM guardian script for the Y arm.

Author: Nathan Holland, A. Mullavey.
Date: 2019-05-08
Contact: nathan.holland@ligo.org

Modified: 2019-05-08 (Created)
Modified: 2019-05-09 (Continued writing first iteration; 
                      Planning in code comments;
                      Minor debugging).
Modified: 2019-05-13 (Continued writing first iteration; 
                      Planning in code comments;
                      Minor debugging).
Modified: 2019-05-14 (Continued writing first iteration; 
                      Planning in code comments;
                      Minor debugging).
Modified: 2019-05-21 (Some clean up, removing path changes).
Modified: 2019-06-03 (Adapt to PEP8/Guardian Style).
Modified: 2019-06-17 (Adjust power levels to using argparse.Namespace objects).
Modified: 2019-06-18 (Debugging and testing.).
"""


#------------------------------------------------------------------------------
#Imports:

# Import the simple namespace class form argparse.
from argparse import Namespace

# Import the class Optic from 
# /opt/rtcds/userapps/release/asc/l1/guardian/optic.py
from optic import Optic

# The node class to manage the ALS arm guardian.
from guardian import Node

from os import path
import csv

# Import the base X/Y arm code.
import ALIGN_ARM

#------------------------------------------------------------------------------

align_dir = '/opt/rtcds/userapps/release/asc/l1/guardian/'

arm = "Y"
tms = "TMS"+arm
itm = "ITM"+arm
etm = "ETM"+arm
pr2 = "PR2"
 
# The arm being aligned.
ALIGN_ARM.arm = arm #'Y'

# The test mass being aligned.
ALIGN_ARM.TMS = tms   #"TMSY"
ALIGN_ARM.ITM = itm   # "ITMY"
ALIGN_ARM.ETM = etm   #"ETMY"

# Convenient access for the optics.
ALIGN_ARM.tms = Optic(tms)  #Optic("TMSY")
ALIGN_ARM.itm = Optic(itm)  #Optic("ITMY")
ALIGN_ARM.etm = Optic(etm)  #Optic("ETMY")
ALIGN_ARM.pr2 = Optic(pr2)

# The ALS arm guardian to manage.
ALIGN_ARM.als_loose = Node("ALS_YARM")

#------------------------------------------------------------------------------
### TMS ###

# The oscillator number for the TMS alignment.
ALIGN_ARM.tms_osc_number = "10"
ALIGN_ARM.tms_osc_row = "12"
ALIGN_ARM.tms_demod_sensor = "9"
ALIGN_ARM.tms_servo_row = "12"

ALIGN_ARM.tms_demod_phase = {"PIT":111,
                             "YAW":-3
                            }

ALIGN_ARM.tms_dof_gain = {"PIT":0.7,
                          "YAW":1.0
                         }

# The optimised alignment values for the TMS bench.
# Approximately points the TMS bench to the ITM baffle PD.
ALIGN_ARM.tms_pit = 5400
ALIGN_ARM.tms_yaw = -1720

filename = "tms{0}_bpd1_offsets.csv".format(arm.lower())
if path.exists(align_dir + filename):
    with open(align_dir + filename,"r") as csvfile:
        reader = csv.DictReader(csvfile)
        list = []
        for row in reader:
            list.append(dict(row))
        bpd1_ofs = list[-1]
    bpd1_ofs["P"] = float(bpd1_ofs["P"])
    bpd1_ofs["Y"] = float(bpd1_ofs["Y"])
    ALIGN_ARM.tms_bpd1_ofs = bpd1_ofs  
else:
    ALIGN_ARM.tms_bpd1_ofs = {'P':26.95,'Y':-8.42} #{'P':27.0,'Y':-8.6}

# Stop, and implicit start, conditions for automatic TMS alignment.
_tms_stop = {"pwr_mn" : 0.2e-3,     # Minimum baffle PD power.
             "pwr_ok" : 1.1e-3, #1.8e-3, # Acceptable baffle PD power.
             "grd_ok" : 1.0e-5,  # Maximum level for power gradient.
             "pit_ok" : 1.0e-4,  # Maximum level for Pit error signal.
             "yaw_ok" : 1.0e-4   # Maximum level for Yaw error signal.
            }
ALIGN_ARM.tms_stop = Namespace(**_tms_stop)

# The gains for the TMS alignment dithering in pitch.
ALIGN_ARM.tms_G_pit = -1.0
ALIGN_ARM.tms_G_yaw = 0.3

#------------------------------------------------------------------------------
### ITM ###

# The optimised alignment values for the ITM.
# Approximately points the ITM to the ETM baffle PD.
# Must be adjusted by the gain of the filter module.
ALIGN_ARM.itm_pit_ofs = -327.0
ALIGN_ARM.itm_yaw_ofs = 832.0

filename = "itm{0}_bpd1_offsets.csv".format(arm.lower())
if path.exists(align_dir + filename):
    with open(align_dir + filename,"r") as csvfile:
        reader = csv.DictReader(csvfile)
        list = []
        for row in reader:
            list.append(dict(row))
        bpd1_ofs = list[-1]
    bpd1_ofs["P"] = float(bpd1_ofs["P"])
    bpd1_ofs["Y"] = float(bpd1_ofs["Y"])
    ALIGN_ARM.itm_bpd1_ofs = bpd1_ofs  
else:
    ALIGN_ARM.itm_bpd1_ofs = {'P':-19.02,'Y':18.94} #{'P':-18.60,'Y':18.59}

# Stop, and implicit start, conditions for automatic TMS alignment.
_itm_stop = {"pwr_mn" : 0.1e-3,     # Minimum baffle PD power.
             "pwr_ok" : 0.5e-3, # Acceptable baffle PD power.
             "grd_ok" : 7.0e-6,  # Maximum level for power gradient.
             "pit_ok" : 7.0e-5,  # Maximum level for Pit error signal.
             "yaw_ok" : 7.0e-5   # Maximum level for Yaw error signal.
            }
ALIGN_ARM.itm_stop = Namespace(**_itm_stop)

# The gains for the ITM alignment dithering.
ALIGN_ARM.itm_G_pit = -0.5
ALIGN_ARM.itm_G_yaw = 0.5

#------------------------------------------------------------------------
### ETM ###
#TODO find correct values

# The optimised alignment values for the ITM.
# Approximately points the ITM to the ETM baffle PD.
# Must be adjusted by the gain of the filter module.

filename = "etm{0}_bpd1_offsets.csv".format(arm.lower())
if path.exists(align_dir + filename):
    with open(align_dir + filename,"r") as csvfile:
        reader = csv.DictReader(csvfile)
        list = []
        for row in reader:
            list.append(dict(row))
        bpd1_ofs = list[-1]
    bpd1_ofs["P"] = float(bpd1_ofs["P"])
    bpd1_ofs["Y"] = float(bpd1_ofs["Y"])
    ALIGN_ARM.etm_bpd1_ofs = bpd1_ofs  
else:
    ALIGN_ARM.etm_bpd1_ofs =  {'P':-17.45,'Y':-18.04} #{'P':-18.60,'Y':-18.59}


# Acceptable power level for final alignment.
_etm_stop = {"pwr_mn" : 0.1e-3,     # Minimum baffle PD power.
             "pwr_ok" : 0.55e-3, # Acceptable baffle PD power.
             "grd_ok" : 7.0e-6,  # Maximum level for power gradient.
             "pit_ok" : 7.0e-5,  # Maximum level for Pit error signal.
             "yaw_ok" : 7.0e-5   # Maximum level for Yaw error signal.
            }
ALIGN_ARM.etm_stop = Namespace(**_etm_stop)

# The gains for the ETM alignment dithering. TODO: find correct gains
ALIGN_ARM.etm_G_pit = 0.5
ALIGN_ARM.etm_G_yaw = 0.5

#------------------------------------------------------------------------
### Arm Cavity Alignment ###


_arm_stop = {"pwr_min" : 600,
             "pwr_aligned" : 1200,
             "pit_ok" : 300.0,
             "yaw_ok" : 300.0,
             "pit_aligned" : 140,
             "yaw_aligned" : 100
            }
ALIGN_ARM.arm_stop = Namespace(**_arm_stop)

# Gains for the TMS to arm cavity servo
ALIGN_ARM.arm_G_pit = -0.05
ALIGN_ARM.arm_G_yaw = -0.1

### Nominal camera centroid values
#ALIGN_ARM.cam_spots_nom = {'ITM':{'P':-24.2,'Y':-46.6},
#                           'ETM':{'P':-94.8,'Y':130.8}}
ALIGN_ARM.cam_spots_nom = {'ITM':{'P':-20.5,'Y':-47.0},
                           'ETM':{'P':-92.8,'Y':133.1}}

ALIGN_ARM.cam_dof_num = {'I':12,'E':14}

# Sus calibrations, TMS is just top stage (gain in opticalign bank),
# ETM and ITM is L1 Lock to M0 OpticAlign
ALIGN_ARM.sus_cals = {'TMS':{'P':1.0/200,'Y':1.0/200},
                      'ITM':{'P':1.8/400,'Y':1.3/400},
                      'ETM':{'P':1.6/400,'Y':1.2/400}
                      }

#------------------------------------------------------------------------------

#Import everthing to make it a Guardian script:
from ALIGN_ARM import *


# Set a prefix, if necessary.
#prefix = ""


# END.
