# Docstring:
"""
Inital Alignment Guardian: Green Arm Cavity Alignment.

This, generic, guardian script is designed to be specialised to align either 
the X, or Y arms using the green ALS laser.

Authors: N. A. Holland, A. Mullavey.
Date: 2019-05-02
Contact: nathan.holland@ligo.org

Modified: 2019-05-02 (Created)
Modified: 2019-05-03 (Continued writing first iteration).
Modified: 2019-05-06 (Continued writing first iteration).
Modified: 2019-05-07 (Continued writing first iteration; 
                      Planning in code comments).
Modified: 2019-05-08 (Continued writing first iteration; 
                      Planning in code comments).
Modified: 2019-05-09 (Continued writing first iteration; 
                      Planning in code comments;
                      Minor debugging).
Modified: 2019-05-13 (Continued writing first iteration; 
                      Planning in code comments;
                      Minor debugging).
Modified: 2019-05-14 (Continued writing first iteration; 
                      Planning in code comments;
                      Minor debugging).
Modified: 2019-05-21 (Debugging and testing).
Modified: 2019-05-22 (Changed structure to make it more self contained).
Modified: 2019-05-24 (Syntatic changes, debugging from lower level API).
Modified: 2019-05-28 (Debugging and testing).
Modified: 2019-06-03 (Adapt to PEP8/Guardian Style).
Modified: 2019-06-04 (Temporarily remove als_loose from automatic control;
                      Adapt to PEP8/Guardian Style).
Modified: 2019-06-07 (Fixed ezca feedthrough for align_restore, and align_save
                      functions).
Modified: 2019-06-11 (Debugging and testing.).
Modified: 2019-06-12 (Added notifications).
Modified: 2019-06-17 (Adjust power levels to using argparse.Namespace objects;
                      Added in tracking of the gradient during automatic
                      alignment steps, for testing purposes).
Modified: 2019-06-18 (Debugging and testing.).
Modified: 2019-06-19 (Improved logging for automatic alignment steps.).
Modified: 2019-06-20 (Modified header logging)
Modified: 2019-06-27 (Corrected transmitted power data collection at ITM
                      alignment.)
Modified: 2019-06-28 (Added stop conditions for TMS and ITM alignment.)
Modified: 2019-07-11 (Bug fix.)
"""


#------------------------------------------------------------------------------
# Imports:

# Import time, for waiting.
import time
# Import gpstime for tagging
import gpstime

# Import the minimum required number of guardian subutilities.
from guardian import GuardState

# Import the LSC matrices access. Source is located at:
# /opt/rtcds/userapps/release/isc/l1/guardian/isclib/matrices.py
import isclib.matrices as matrix

# Import the classes DigitalDemodulation, and AlignmentDither from
# /opt/rtcds/userapps/release/asc/l1/scripts/align_dither.py
from align_dither import DigitalDemodulation, AlignmentDither

from armSpotCentering import CentroidCentering

# Import the function align_restore from
# /opt/rtcds/userapps/release/sus/common/scripts/align_restore.py
from new_align_restore import align_restore

# Import the function align_save from
# /opt/rtcds/userapps/release/sus/common/scripts/align_save.py
from new_align_save import align_save

# Import the class Tracker from
# /opt/rtcds/userapps/release/asc/l1/guardian/gradient_tracker.py
from gradient_tracker import Tracker

# Running average, for checking that alignment doesn't deteriorate.
from runningAvg import RunningAvg

# Import the EzAvg class from
# /opt/rtcds/userapps/release/isc/l1/guardian/isclib/epics_average.py
# Replaces runningAvg above (which isn't really a running average)
from isclib.epics_average import EzAvg, ezavg

import numpy as np
import awg

#------------------------------------------------------------------------------
#Script variables:

# Define the nominal state for this guardian script.
nominal = "ARM_ALIGN_IDLE"
request = nominal

#------------------------------------------------------------------------------
#				 Functions
#------------------------------------------------------------------------------

#tms_osc_freq = {"PIT" : 2.55,
#               "YAW" : 3.95,
#               }
tms_osc_freq = {"X":{"PIT" : 2.3,"YAW" : 3.5},
                "Y":{"PIT" : 2.9,"YAW" : 4.1}
               }
# NOTE: Better attenuation of omega terms at frequencies that are multiples of 0.5Hz

# drive mtrx for the arm cavity axis
#drive_mtrx = {'P':[[-1.07,1.0],
#                     [-1.0,0.78]],
#                'Y':[[1.07,1.0],
#                     [-1.0,-0.78]]
#               }

#tms_osc_row = matrix.asc_ads_actuators_dither[TMS]

#TODO: For matrices use matrix library, clear rows and columns

def turn_on_tms_osc(ampl=200):
    for dof in ["PIT","YAW"]:
        ezca.write(":ASC-ADS_{0}{1}_OSC_TRAMP".format(dof,tms_osc_number), 5)
        time.sleep(0.1)
        ezca.write(":ASC-ADS_{0}{1}_OSC_FREQ".format(dof,tms_osc_number), tms_osc_freq[arm][dof])
        ezca.write(":ASC-ADS_{0}{1}_OSC_CLKGAIN".format(dof,tms_osc_number), ampl)
        ezca.write(":ASC-ADS_LO_{0}_MTRX_{1}_{2}".format(dof,tms_osc_row,tms_osc_number), 1)

def turn_off_tms_osc():
    # Turn off the TMS pitch and yaw oscillators.
    for dof in ['PIT','YAW']:
        ezca.write(":ASC-ADS_{0}{1}_OSC_TRAMP".format(dof,tms_osc_number), 5)
        time.sleep(0.1)
        ezca.write(":ASC-ADS_{0}{1}_OSC_FREQ".format(dof,tms_osc_number), 0)
        ezca.write(":ASC-ADS_{0}{1}_OSC_CLKGAIN".format(dof,tms_osc_number), 0)
        ezca.write(":ASC-ADS_LO_{0}_MTRX_{1}_{2}".format(dof,tms_osc_row,tms_osc_number), 0)
        #TODO: turn on dither banks as well

def set_tms_demod():
    for dof in ["PIT","YAW"]:
        ezca.write(":ASC-ADS_{}_SEN_MTRX_{}_{}".format(dof,tms_osc_number,tms_demod_sensor), 1)
        ezca.switch(":ASC-ADS_{}{}_DEMOD_SIG".format(dof,tms_osc_number),"FM1","ON")
        ezca.write(":ASC-ADS_{}{}_DEMOD_PHASE".format(dof,tms_osc_number), tms_demod_phase[dof])
        ezca.write(":ASC-ADS_{0}{1}_OSC_SINGAIN".format(dof,tms_osc_number), 1)
        ezca.write(":ASC-ADS_{0}{1}_OSC_COSGAIN".format(dof,tms_osc_number), 1)

def set_tms_servo():
    for dof in ["PIT","YAW"]:
        ezca.switch(":ASC-ADS_{}{}_DOF".format(dof,tms_osc_number),"FM1","OUTPUT","ON")
        ezca.write(":ASC-ADS_{}{}_DOF_GAIN".format(dof,tms_osc_number),tms_dof_gain[dof])
        ezca.write(":ASC-ADS_OUT_{}_MTRX_{}_{}".format(dof,tms_servo_row,tms_osc_number),1)


def spiral_waveforms(a, f1, f2, f_sample, t_duration):
    t_sample = 1.0/f_sample
    t = np.arange(0, t_duration, t_sample)
    r = (2*a/np.pi)*np.arcsin(np.sin(2*np.pi*f2*t))
    theta = 2*np.pi*f1*t
    sgn = np.sign(r)
    X = sgn*r*np.sin(theta)
    Y = r*np.cos(theta)
    return X,Y


# A function for finding peaks above some threshold, scipy has find_peaks
# but it's not available in the current installed version of scipy.
# Also this is copied from scipy signal ......
def peak_finder(data,threshold=0):

    ind_peaks = []
    val_peaks = []

    i = 1  # Pointer to current sample, first one can't be maxima
    i_max = data.shape[0] - 1  # Last sample can't be maxima

    while i < i_max:
        # Test if previous sample is smaller
        if data[i - 1] < data[i]:
            i_ahead = i + 1  # Index to look ahead of current sample

            # Find next sample that is unequal to x[i]
            while i_ahead < i_max and data[i_ahead] == data[i]:
                i_ahead += 1

            # Maxima is found if next unequal sample is smaller than x[i]
            if data[i_ahead] < data[i]:
                left_edge = i
                right_edge = i_ahead - 1
                midpoint = (left_edge + right_edge) // 2
                if data[midpoint]>threshold:
                    ind_peaks.append(midpoint)
                    val_peaks.append(data[midpoint])
                # Skip samples that can't be maximum
                i = i_ahead
        i += 1

    peaks = np.array(val_peaks,ind_peaks)

    return peaks

#------------------------------------------------------------------------------
#                                 Guardian States
#------------------------------------------------------------------------------

# Initial state, always defined, even if not provided by the
# coder.
class INIT(GuardState):  
    request = False
    index = 0
    
    # Main class method - does nothing.
    def main(self):
        # Succeeds.
        return True

#------------------------------------------------------------------------------
# Idle state, which intentionally does little. Used for waiting for
# commands or recovery.
class ARM_ALIGN_IDLE(GuardState):
    #goto = True
    request = True
    index = 2
    
    # Main class method - releases the ALS arm guardian.
    def main(self):
        # Releases the ALS arm guardian.
        #als_loose.release()
        
        # Set a timer to wait, the idea is to let the ALS navigate by
        # itself.
        self.timer["wait"] = 2

    # Run class method - does nothing.
    def run(self):
        # Check for timer expiration.
        if self.timer["wait"]:
            # Timer has expired so proceed.
            return True

#------------------------------------------------------------------------------
# Reset state, undoes any changes to optics, filters and matrices.
class ARM_ALIGN_RESET(GuardState):
    goto = True
    #request = True
    index = 1
    
    # Main class method - reset.
    def main(self):
        # Reset masses:
        for dof in ['P', 'Y']:
            #Remove offsets from the optics.
            itm.align(dof, ezca)
            etm.align(dof, ezca)
            tms.align(dof, ezca)
        
        # Restore the saved values for the masses.
        #align_restore(ITM, ezca)
        #align_restore(ETM, ezca)
        #align_restore(TMS, ezca)

        turn_off_tms_osc()

        # Switch off TMS test filter offsets.
        ezca.switch(":SUS-TMS{0}_M1_TEST_P".format(arm), "OFFSET", "OFF")
        ezca.switch(":SUS-TMS{0}_M1_TEST_Y".format(arm), "OFFSET", "OFF")
        
        # Turn off ITM test filter offsets.
        ezca.switch(":SUS-ITM{0}_M0_TEST_P".format(arm), "OFFSET", "OFF")
        ezca.switch(":SUS-ITM{0}_M0_TEST_Y".format(arm), "OFFSET", "OFF")
        
        # Turn off ETM test filter offsets.
        ezca.switch(":SUS-ETM{0}_M0_TEST_P".format(arm), "OFFSET", "OFF")
        ezca.switch(":SUS-ETM{0}_M0_TEST_Y".format(arm), "OFFSET", "OFF")
        
        # Reset the optic align ramp time for the TMS.
        tms.optic_align_ramp('P', 1, ezca)
        tms.optic_align_ramp('Y', 1, ezca)
        
        # Reset the optic align ramp time for the ITM.
        itm.optic_align_ramp('P', 1, ezca)
        itm.optic_align_ramp('Y', 1, ezca)
        
        # Reset the optic align ramp time for the ETM.
        etm.optic_align_ramp('P', 1, ezca)
        etm.optic_align_ramp('Y', 1, ezca)
        
        # Set the ALS guardian to be managed by this guardian.
        # Why do this here?
        #als_loose.set_managed()
        
        # Check the state of the ALS guardian.
        if als_loose.state == "RELEASE_GREEN_PHOTONS":
            # This is an expected state, so do nothing.
            pass
        elif als_loose.state == "QPDS_LOCKED":
            # This is also an expected state, so do nothing.
            pass
        else:
            als_loose.set_request("QPDS_LOCKED")
        
        # Start a 5s timer, to allow changes to take effect.
        self.timer["align_reset"] = 5

    # Run class method - allows timer to expire.
    def run(self):
        # If timer hasn't expired.
        if not self.timer["align_reset"]:
            # Forbid exit.
            return False
        
        # Check the state of the ALS.
        #if (als_loose.state == "RELEASE_GREEN_PHOTONS") and als_loose.done:
        #    # The ALS is in an allowable state. Succeed.
        #    return True
        #elif (als_loose.state == "QPDS_LOCKED") and als_loose.done:
        #    # The ALS is in an allowable state. Succeed.
        #    return True
        #elif (als_loose.state == "UNLOCK_PDH_GOTO") and als_loose.done:
        #    # The ALS has arrived at UNLOCK_PDH_GOTO. Neither of the
        #    # arrived, or completed methods are appropriate because I
        #    # am about to make a request of the ALS guardian. Release
        #    # the green light - fly my pretties.
        #    als_loose.set_request("RELEASE_GREEN_PHOTONS")
        #elif als_loose.check_fault():
        #    # The ALS guardian is in fault, attempt to correct.
        #    als_loose.revive()
        #else:
        #    # Inform the operator that the ALS guardian is
        #    # transitioning.
        #    log("ALS Guardian is transitioning.")
        #    notify("ALS Guardian is transitioning.")
    
        #    # Forbid exit.
        #    return False
        return True

#------------------------------------------------------------------------------
# Green to TMS Error state - The ALS guardian is not in the QPDs locked state
class GRN_NOT_LOCKED_TO_TMS(GuardState):
    request = False
    index = 7

    # Main class method
    def run(self):
        if als_loose.state != "QPDS_LOCKED":
            notify("ALS_{0}ARM guardian is not in \"QPDS_LOCKED\".".format(arm))
            return False
        elif not als_loose.completed:
            notify("ALS_{0}ARM guardian has not completed.".format(arm))
            # Don't proceed until it is complete.
            return False
        
        return True

#------------------------------------------------------------------------------
# Acquire Green state - Use the ALS to align the green light to the
# TMS bench.
class ALIGN_GRN_TO_TMS(GuardState):
    request = False
    index = 9
    
    # Main class method - send instructions.
    def main(self):
        # Request the ALS guardian lock the green to the TMS.
        als_loose.set_request("QPDS_LOCKED")
        
        # Set a timer, for periodic checking.
        self.timer["check"] = 2.0
        # Set a time out timer, for a maximum wait time.
        self.timer["max_wait"] = 60.0

    # Run class method - Appropriate flow control.
    def run(self):
        # If the timer, for checking, has expired.
        if self.timer["check"]:
            # Check to see if the ALS has arrived at the requested state.
            if als_loose.arrived:
                # The ALS has arived, so you may proceed.
                return True
            # The ALS has not arrived.
            else:
                # Reset the 2s timer.
                self.timer["check"] = 2.0
        
        # If the timer, for maximum timeout, has expired.
        if self.timer["max_wait"]:
            # Warn the operator that the TMS failed to acquire the
            # green light.            
            log("ALS_{0}ARM guardian failed to reach ".format(arm) + \
                "\"QPDS_LOCKED\" within 60 seconds (timeout).")
            notify("ALS_{0}ARM guardian failed to reach ".format(arm) + \
                   "\"QPDS_LOCKED\" within 60 seconds (timeout).")
            
            # Return the error state, to restart alignment.
            return "GRN_NOT_LOCKED_TO_TMS"

#------------------------------------------------------------------------------
# Green Acquired state - The ALS has acquired the green light to the
# TMS bench.
class GRN_ALIGNED_TO_TMS(GuardState):
    request = True
    index = 10
    
    #TODO: If QPDs don't lock, ALS might enter error state, which might stall this guardian
    # Could revive in GRN_NOT_LOCKED_TO_TMS state
    # Run class method - Check that the ALS is still acquired.
    def run(self):
        # The QPDS_LOCKED state has not been completed.
        if not als_loose.completed:
            # Don't proceed until it is complete.
            return False
        # The ALS is not in the requested state, for some reason.
        elif als_loose.state != "QPDS_LOCKED":
            # Inform the operator that an error has occurred.
            #emessage = "ALS_{0}ARM guardian is not in \"QPDS_LOCKED\".".format(arm)
            log("ALS_{0}ARM guardian is not in \"QPDS_LOCKED\".".format(arm))
            notify("ALS_{0}ARM guardian is not in \"QPDS_LOCKED\".".format(arm))
            
            # Return the error state, to restart alignment.
            return "GRN_NOT_LOCKED_TO_TMS"
        # Assume no faults have occurred otherwise.
        else:
            # Success.
            return True



#------------------------------------------------------------------------------
# Misalignment state - Prevents IR or GRN cavities from being resonant
# which may provide spurious error signals.
class MISALIGN_FOR_GRN_CAV(GuardState):
    request = True
    index = 15
    
    # Main class method - misalign the ETM.
    def main(self):
        # Inform the operator.
        log("Misaligning {0}.".format(ETM))
        
        # Misalign the ETM.
        etm.misalign('P', -40, ezca)
        if not pr2.is_misaligned('P',ezca):
            pr2.misalign('P', -500, ezca)

        # TODO: Ensure PR2 is misaligned. Tricky because could cause race condition.
        # TODO: Implement this.
        # Record the gain.
        #etm_P_G = ezca.read(":SUS-ETM{0}_M0_OPTICALIGN_P_GAIN".format(arm))
        #etm_Y_G = ezca.read(":SUS-ETM{0}_M0_OPTICALIGN_Y_GAIN".format(arm))
        # Misalign the ETM away from the ITM baffle PD.
        #etm.misalign('P', -etm_pit_ofs / etm_P_G, ezca)
        #etm.misalign('Y', -etm_yaw_ofs / etm_Y_G, ezca)
   
        # Set the TMS oscillator ramp times.
        ezca.write(":ASC-ADS_PIT{0}_OSC_TRAMP".format(tms_osc_number), 3)
        ezca.write(":ASC-ADS_YAW{0}_OSC_TRAMP".format(tms_osc_number), 3)

        # Set a 1s timer.
        self.timer["wait"] = 1.0

    # Run class method - wait for the timer to expire
    def run(self):
        # If timer has expired.
        if self.timer["wait"]:
            # Allow exit.
            return True


#------------------------------------------------------------------------------
class POINT_TMS_TO_ITM_BFFL_PD(GuardState):
    request = False
    index = 16

    def main(self):

        # Inform the operator.
        log("Aligning TMS{0} to ITM{0} Baffle PD 1.".format(arm))

        #------------------------------------------------------- 
        # Ramp the offsets to pre-saved values over 10 seconds.

        # Set TMS optic align ramp times.
        tms.optic_align_ramp('P', 1, ezca)
        tms.optic_align_ramp('Y', 1, ezca)
        #tms.test_ramp('P', 10, ezca)
        #tms.test_ramp('Y', 10, ezca)

        ## Turn the offsets and outputs on.
        tms.misalign('P', tms_bpd1_ofs['P'], ezca)
        tms.misalign('Y', tms_bpd1_ofs['Y'], ezca)
        #------------------------------------------------------- 

        self.timer['wait'] = 5

        self.bffl_pd = EzAvg(ezca,3,":AOS-ITM{0}_BAFFLEPD_1_POWER".format(arm))

    def run(self):

        if not self.timer['wait']:
            return

        [done,val] = self.bffl_pd.ezAvg()
        if not done:
            return
        elif val > tms_stop.pwr_mn:
            log('Sufficient power in Baffle PD, will begin optimization')
            return 'ALIGN_TMS_TO_ITM_BFFL'
        else:
            log('Baffle PD light too low, will scan for it')
            return 'FIND_ITM_BFFL_PD_WITH_TMS'

        return True


#------------------------------------------------------------------------------
def offload_test_alignment(sus,dof,test_output,tramp=3.0):
    if sus == 'TMS':
        stage = 'M1'
    else:
        stage = 'M0'
    oagain = ezca['SUS-{}{}_{}_OPTICALIGN_{}_GAIN'.format(sus,arm,stage,dof)]
    #test_output = ezca['SUS-{}{}_{}_TEST_{}_OUTPUT'.format(sus,arm,stage,dof)]
    offset = test_output/oagain
    if ezca.LIGOFilter('SUS-{}{}_{}_TEST_{}'.format(sus,arm,stage,dof)).is_engaged('OFFSET'):
        offset -= ezca['SUS-{}{}_{}_TEST_{}_OFFSET'.format(sus,arm,stage,dof)]
    ezca['SUS-{}{}_{}_OPTICALIGN_{}_TRAMP'.format(sus,arm,stage,dof)] = tramp
    time.sleep(0.1)
    ezca['SUS-{}{}_{}_OPTICALIGN_{}_OFFSET'.format(sus,arm,stage,dof)] += offset
    ezca.switch('SUS-{}{}_{}_TEST_{}'.format(sus,arm,stage,dof),'HOLD','OFF')


def make_bfflpdfinder_state(sus,driveAmpl=5.0,driveFreq1=0.1,driveFreq2=0.002,duration=125):
    # driveFreq1 = frequency of sine waves, driveFreq2 = frequency of envelope (triangle wave)
    class FIND_BFFL_PD(GuardState):
        request = False

        def main(self):

            # Set test gains, and put in offsets to align to BPD
            if sus == 'TMS':
                tms.misalign('P', tms_bpd1_ofs['P'], ezca)
                tms.misalign('Y', tms_bpd1_ofs['Y'], ezca)

                self.thresh_hi = tms_stop.pwr_mn
                self.thresh_lo = tms_stop.pwr_ok

            elif sus == 'ITM':
                itm.misalign('P', itm_bpd1_ofs['P'], ezca)
                itm.misalign('Y', itm_bpd1_ofs['Y'], ezca)

                self.thresh_hi = itm_stop.pwr_mn
                self.thresh_lo = itm_stop.pwr_ok

            elif sus == 'ETM':
                etm.misalign('P', etm_bpd1_ofs['P'], ezca)
                etm.misalign('Y', etm_bpd1_ofs['Y'], ezca)

                self.thresh_hi = etm_stop.pwr_mn
                self.thresh_lo = etm_stop.pwr_ok


            # Channels
            self.chan = {}
            if sus=="TMS":
                self.stage = "M1"
            else:
                self.stage = "M0"

            for dof in ["P","Y"]:
                self.chan[dof] = 'SUS-'+sus+arm+'_'+self.stage+'_TEST_'+dof+'_'

            if sus == 'ITM':
                self.bafflePdChan = ':AOS-ETM{0}_BAFFLEPD_1_POWER'.format(arm)
            else:
                self.bafflePdChan = ':AOS-ITM{0}_BAFFLEPD_1_POWER'.format(arm)


            #driveAmpl = 1.5 #1.0	#max amplitude (amplitude of triangle wave)
            #driveFreq1 = 0.1 #0.05		#frequency of sine waves
            #driveFreq2 = 0.002 #0.004	#frequency of envelope (triangle wave)
            self.f_s = 16384		#sampling frequency
            #duration = 125

            #Generate waveforms
            self.driveYaw,self.drivePit = spiral_waveforms(driveAmpl,driveFreq1,driveFreq2,self.f_s,duration)

            self.ii = 0

            self.tappend = 1.0
            #self.step = self.tappend*f_s #FIXME: this needs to be the number of steps (=fs)
            self.step = self.f_s
            self.append_stream = True
            self.scan_started = False
            self.savedata = True
            self.monitor = True
            self.offload_alignment = False
            self.set_up_stream = True
            self.set_from_scan = True
            self.close_stream = True

            #self.threshold = 

        def run(self):

            if self.set_up_stream:
                #Tuple the waveform
                self.wforms = (self.drivePit,self.driveYaw)

                #Arrays for data
                self.bafflePD = np.array([])
                self.drivePitTestOut = np.array([])
                self.driveYawTestOut = np.array([])

                tnow = int(gpstime.tconvert('now'))
                self.tstart = tnow + 10
                self.tnext = self.tstart-1
                log("Start time is {}".format(int(self.tstart)))
                log("Opening awg streams")

                self.exc = []
                for dof in ["P","Y"]:
                    self.exc.append(awg.ArbitraryStream( IFO+":"+self.chan[dof]+"EXC",rate=self.f_s, start=self.tstart , appinfo="two stream"))
                for ex in self.exc:
                    ex.open()

                self.set_up_stream = False

            if self.append_stream:
                dt = self.tnext - self.tstart
                log("Time from t0: %d" % dt )
                end = min(self.ii+self.step, len(self.wforms[0]))
                #Append next part of waveforms to streams
                for exnum in range(len(self.exc)):
                    self.exc[exnum].append(self.wforms[exnum][self.ii:end])

                self.ii += self.step
                self.append_stream = False

            if (self.monitor and int(gpstime.tconvert('now')) < self.tnext):
                if not self.scan_started: 
                    if gpstime.tconvert('now')>(self.tstart-0.2):
                        log("Scanning and recording data, time is {0}".format(gpstime.tconvert('now')))
                        self.scan_started = True
                    time.sleep(0.2)
                    return
                elif ezca[self.bafflePdChan] > self.thresh_hi:
                    log('Above threshold, hold')
                    ezca.switch(':SUS-{}{}_{}_TEST_P'.format(sus,arm,self.stage),'HOLD','ON')
                    ezca.switch(':SUS-{}{}_{}_TEST_Y'.format(sus,arm,self.stage),'HOLD','ON')
                    for exnum in range(len(self.exc)):
                        self.exc[exnum].abort()
                        self.exc[exnum].close()
                    self.monitor = False
                    self.offload_alignment = True
                    self.set_from_scan = False
                    return
                else:
                    self.bafflePD = np.append(self.bafflePD, ezca[self.bafflePdChan])
                    self.drivePitTestOut = np.append(self.drivePitTestOut, ezca[self.chan["P"]+'OUTPUT'])
                    self.driveYawTestOut = np.append(self.driveYawTestOut, ezca[self.chan["Y"]+'OUTPUT'])
                    return
            elif (self.monitor and self.ii<len(self.wforms[0])):
                self.tnext += self.tappend
                self.append_stream = True
                return

            log("Finished scan")

            #abort and close stream
            if self.close_stream:
                for exnum in range(len(self.exc)):
                    self.exc[exnum].abort()
                    self.exc[exnum].close()
                self.close_stream = False

            if self.offload_alignment:
                log('Offloading TEST outputs to OPTICALIGN offsets')
                test_outputs = {}
                for dof in ['P','Y']:
                    test_outputs[dof] = ezca[':SUS-{}{}_{}_TEST_{}_OUTPUT'.format(sus,arm,self.stage,dof)]
                offload_test_alignment(sus,'P',test_outputs['P'],tramp=3.0)
                offload_test_alignment(sus,'Y',test_outputs['Y'],tramp=3.0)
                time.sleep(3)
                self.offload_alignment=False

            if self.set_from_scan and np.max(self.bafflePD) > self.thresh_lo:
                jj = np.argmax(self.bafflePD)
                offload_test_alignment(sus,'P',self.drivePitTestOut[jj],tramp=3.0)
                offload_test_alignment(sus,'Y',self.driveYawTestOut[jj],tramp=3.0)
                self.set_from_scan = False


            if self.savedata:
                filedir = '/data/ASC/InitialAlignment/'+arm+'ARM/'
                filename = filedir + '{}{}_scanForBafflePD_{}.npz'.format(sus.lower(),arm.lower(),int(self.tstart))
                log("Saving data to {}".format(filename))
                np.savez(filename, bafflePD=self.bafflePD, drivePitTestOut=self.drivePitTestOut, driveYawTestOut=self.driveYawTestOut)
                self.savedata = False

            return True

    return FIND_BFFL_PD

#------------------------------------------------------------------------------
# Find ITM baffle PD with TMS state, spiral scans the TMS, this is 
# for when it's not close enough to align using dither.

FIND_ITM_BFFL_PD_WITH_TMS = make_bfflpdfinder_state('TMS')
FIND_ITM_BFFL_PD_WITH_TMS.index = 17

#------------------------------------------------------------------------------
# TMS Alignment state - Automatically aligns the TMS to the ITM baffle
# PD.
class ALIGN_TMS_TO_ITM_BFFL(GuardState):
    request = False
    index = 19
    
    # Main class method - Setup for the automated alignment procedure.
    def main(self):
        # Inform the operator.
        log("Aligning TMS{0} to ITM{0} Baffle PD 1.".format(arm))

        #------------------------------------------------------- 
        # Ramp the offsets to pre-saved values over 10 seconds.

        # Set TMS optic align ramp times.
        tms.optic_align_ramp('P', 1, ezca)
        tms.optic_align_ramp('Y', 1, ezca)
        #tms.test_ramp('P', 10, ezca)
        #tms.test_ramp('Y', 10, ezca)

        ## Turn the offsets and outputs on.
        tms.misalign('P', tms_bpd1_ofs['P'], ezca)
        tms.misalign('Y', tms_bpd1_ofs['Y'], ezca)
        #------------------------------------------------------- 

        # Enable the TMS Pitch oscillator at 2.8 Hz.
        turn_on_tms_osc()

        # Instantiate the digital demodulator. Used to
        # provide sensor of how well the TMS is aligned
        # to the baffle pd
        self.dig_demod = DigitalDemodulation(ezca,
            ":AOS-ITM{0}_BAFFLEPD_1_POWER".format(arm),
            ":SUS-TMS{0}_M1_DITHER_P_INMON".format(arm),
            ":SUS-TMS{0}_M1_DITHER_Y_INMON".format(arm))

        ### Instantiate classes for servoing TMS to baffle pd
        # The alignment actuator for pitch.
        self.p_ad = AlignmentDither(ezca, 
            ":SUS-TMS{0}_M1_OPTICALIGN_P_OFFSET".format(arm), tms_G_pit)
        # The alignment dither actuator for yaw.
        self.y_ad = AlignmentDither(ezca, 
            ":SUS-TMS{0}_M1_OPTICALIGN_Y_OFFSET".format(arm), tms_G_yaw)

        # Create a tracker object for the averaged power.
        self.power_gradient = Tracker()
        
        # A flag for the first iteration.
        self.first = True
        # A counter for tracking successful iterations.
        self.counter = 0
        # A timer for maximum timeout.
        self.timer["timeout"] = 130
        # Set a timer, to allow changes to take effect.
        self.timer["wait"] = 10
        self.timer["move-on"] = 45

        # Filename for writing data to
        #pathname = '/opt/rtcds/userapps/trunk/asc/l1/guardian/data/'
        pathname = "/data/ASC/InitialAlignment/TMS{0}/".format(arm)
        filetime = str(int(gpstime.gpsnow()))
        filename = "tms{0}_to_itm{0}_bpd".format(arm.lower())
        self.fname = pathname + filename + '_' + filetime + '.txt'

        # Make data arrays
        #self.gtimeArr = np.array([])
        #self.PDpwerArr = np.array([])
        #self.pitDemodArr = np.array([])
        #self.yawDemodArr = np.array([])
        #self.pGradArr = np.array([])
        #self.countArr = np.array([])
        
    # Run class method - Collect data and actuate.
    def run(self):
        # Wait for changes to take effect
        if not self.timer["wait"]:
            # Don't proceed until the timer elapses.
            return False

        # Collect the data and CIC filter it.
        PD_pwr, Pit_demod, Yaw_demod = self.dig_demod.demod()
        
        # Save the value for the gradient tracking.
        self.power_gradient.value = PD_pwr

        # ajm20191025
        # Check that the power on the BFFL PD is high enough.
        #if PD_pwr < tms_stop.pwr_mn:
        #    # Inform the operator that the power is too low.
        #    log("Green power on ITM{0} Baffle PD 1, too low.".format(arm))
        #    notify("Green power on ITM{0} Baffle PD 1, too low.".format(arm))
        #
        #    # Jump to the manual/scan state.
        #    return "FIND_ITM_BFFL_PD_WITH_TMS"

        no_grad = False
        # Check for first iteration.
        if self.first:
            # Set the flag to False, so that it won't run again.
            self.first = False
            no_grad = True
            # Log a header for the DATA.
            header = "GPSTIME\tAVG-TRANS\tPIT-ERROR\tYAW-ERROR\tGRAD-POWER\n"
            with open (self.fname, "a") as f:
                f.write(header)

        # You can check the stop conditions on subsequent passes.
        else:
            # First assume that step was successful.
            self.counter += 1
            
            # TODO: Think about making flags here instead
            # Check the stop conditions.
            if abs(self.power_gradient.gradient) > tms_stop.grd_ok:
                self.counter = 0
                log("|Gradient of average green power|, too high.")
            
            if abs(Pit_demod) > tms_stop.pit_ok:
                self.counter = 0
                log("|Pitch error|, too large.")
            
            if abs(Yaw_demod) > tms_stop.yaw_ok:
                self.counter = 0
                log("|Yaw error|, too large.")

            if abs(PD_pwr) < tms_stop.pwr_ok:
                self.counter = 0
                log("Baffle PD Power too low")

            # Wait for final stop conditions to be met for 3 iterations.
            #if (self.counter >= 3):
            #    # We are done.
            #    return True
            #else:
            #    pass

            # Uncomment this and comment out above 'return True'), if wanting to make it a timed thing
            if self.timer["move-on"]:
                return True

        # ajm20191025 commented out 
        # Check for timeout.
        #if self.timer["timeout"]:
        #    # Inform the operator.
        #    log("Timeout on TMS{0} alignment.".format(arm))
        #    notify("Timeout on TMS{0} alignment. ".format(arm) + \
        #           "Please manually check/confirm alignment. " + \
        #           "You may rerun this step, if deemed appropriate.")
        #    
        #    # Jump to the manual/scan state.
        #    return "FIND_ITM_BFFL_PD_WITH_TMS"

        # Actuate the TMS.
        # Default normalisation of 1 is used.
        self.p_ad.align_dither(Pit_demod, PD_pwr)
        self.y_ad.align_dither(Yaw_demod, PD_pwr)

        if no_grad:
            p_gradient = 0
        else:
            p_gradient = self.power_gradient.gradient

        # write important data to file for analysis
        gtime = gpstime.gpsnow()
        data = "{0:.3f}\t{1:.3e}\t{2:.3e}\t{3:.3e}\t{4:.3e}\t{5}\n".format(gtime,PD_pwr,Pit_demod,Yaw_demod,p_gradient,self.counter)
        with open (self.fname, "a") as f:
            f.write(data)


#------------------------------------------------------------------------------
# TMS Aligned state - Ensures the TMS is aligned to the ITM baffle.
class TMS_ALIGNED_TO_ITM(GuardState):
    request = True
    index = 20

    # Main class method - Turn off the oscillators.
    def main(self):

        # Turn off the TMS pitch and yaw oscillators.
        turn_off_tms_osc()

        # Turn off test bank offsets
        ezca.switch("SUS-TMS{0}_M1_TEST_P".format(arm), "OFFSET", "OFF")
        ezca.switch("SUS-TMS{0}_M1_TEST_Y".format(arm), "OFFSET", "OFF")

        # Save the offset on the TMS.
        #align_save(TMS, ezca)

    def run(self):

        return True


#------------------------------------------------------------------------------
class POINT_ITM_TO_ETM_BFFL_PD(GuardState):
    request = False
    index = 21

    def main(self):

        # Inform the operator.
        log("Aligning ITM{0} to ETM{0} Baffle PD 1.".format(arm))

        #------------------------------------------------------- 
        # Ramp the offsets to pre-saved values over 10 seconds.

        # Set ITM optic align ramp times.
        itm.optic_align_ramp('P', 1, ezca)
        itm.optic_align_ramp('Y', 1, ezca)

        ## Turn the offsets and outputs on.
        itm.misalign('P', itm_bpd1_ofs['P'], ezca)
        itm.misalign('Y', itm_bpd1_ofs['Y'], ezca)
        #------------------------------------------------------- 

        self.timer['wait'] = 5

        self.bffl_pd = EzAvg(ezca,3,":AOS-ETM{0}_BAFFLEPD_1_POWER".format(arm))

    def run(self):

        if not self.timer['wait']:
            return

        [done,val] = self.bffl_pd.ezAvg()
        if not done:
            return
        elif val > itm_stop.pwr_mn:
            log('Sufficient power in Baffle PD, will begin optimization')
            return 'ALIGN_ITM_TO_ETM_BFFL'
        else:
            log('Baffle PD light too low, will scan for it')
            return 'FIND_ETM_BFFL_PD_WITH_ITM'

        return True

#------------------------------------------------------------------------------
# Find ITM baffle PD with TMS state, spiral scans the TMS, this is 
# for when it's not close enough to align using dither.

FIND_ETM_BFFL_PD_WITH_ITM = make_bfflpdfinder_state('ITM')
FIND_ETM_BFFL_PD_WITH_ITM.index = 22

'''
# Manual/Scan ITM state - Allow the operator to manually scan the ITM.
# TODO: In future implement an automatic scan, for at least a limited
# time.
class FIND_ETM_BFFL_PD_WITH_ITM(GuardState):
    request = False
    index = 27

    # Main class method - Setup for manual/scan alignment.
    def main(self):
        # Digital demodulation for monitoring progress. Note: only need to monitor power, not demod vals
        self.dig_demod = DigitalDemodulation(ezca,
            ":AOS-ETM{0}_BAFFLEPD_1_POWER".format(arm),
            ":SUS-TMS{0}_M1_LOCK_P_INMON".format(arm),
            ":SUS-TMS{0}_M1_LOCK_Y_INMON".format(arm))

        # Turn off the pitch oscillator. 
        ezca.write(":ASC-ADS_PIT{0}_OSC_CLKGAIN".format(tms_osc_number), 0)
        # Turn off the yaw oscillator.
        ezca.write(":ASC-ADS_YAW{0}_OSC_CLKGAIN".format(tms_osc_number), 0)

        # Set a 15 s timer to remind the operator.
        self.timer["remind"] = 15.0

    # Run class method - Run the manual alignment.
    def run(self):
        # Collect data, but discard the non-power measurements.
        demd = self.dig_demod.demod()
        PD_pwr = demd[0]

        # The timer has expired.
        if self.timer["remind"]:
            #Remind the operator to manually align the ITM.
            log("Green power on ETM{0} Baffle PD 1, too low. ".format(arm) + \
                "Please manually align ITM{0} to ETM{0} ".format(arm) + \
                "baffle 1 PD.")
            notify("Green power on ETM{0} Baffle PD 1, too low".format(arm) + \
                   ". Please manually align ITM{0} to ETM{0} ".format(arm) + \
                   "baffle 1 PD.")

            #Reset the timer.
            self.timer["remind"] = 15.0

        # Check the power level.
        if PD_pwr >= itm_stop.pwr_mn:
            # Inform the operator that automatic ITM alignment is
            # resuming.
            log("Green power on ETM{0} Baffle PD 1 is sufficient.".format(arm))
            notify("Green power on ETM{0} Baffle PD 1 is ".format(arm) + \
                   "sufficient.")

            # Return to the automatic alignment, it is the only way
            # out of this state (other than error jumps).
            return True
        
        # Otherwise the state has not completed.
        return False
'''


#------------------------------------------------------------------------------
# ITM Alignment state - Align the ITM to the ETM baffle.
class ALIGN_ITM_TO_ETM_BFFL(GuardState):
    request = False
    index = 24   
    
    # Main class method - Set up the automatic alignment.
    def main(self):

        # Inform the operator.
        log("Aligning ITM{0} to ETM{0} Baffle PD 1.".format(arm))

        # Enable the TMS pitch oscillator.
        turn_on_tms_osc()

        #-------------------------------------------------
        #Align ITM to Baffle PD:
        #-------------------------------------------------
        itm.misalign('P', itm_bpd1_ofs['P'], ezca)
        itm.misalign('Y', itm_bpd1_ofs['Y'], ezca)
        #-------------------------------------------------

        itm.optic_align_ramp('P', 1, ezca)
        itm.optic_align_ramp('Y', 1, ezca)

        # Digital demodulation, CIC filter the data.
        self.dig_demod = DigitalDemodulation(ezca,
            ":AOS-ETM{0}_BAFFLEPD_1_POWER".format(arm),
            ":SUS-TMS{0}_M1_DITHER_P_INMON".format(arm),
            ":SUS-TMS{0}_M1_DITHER_Y_INMON".format(arm))

        # Alignment dither actuator for the pitch.
        self.p_ad = AlignmentDither(ezca,
            ":SUS-ITM{0}_M0_OPTICALIGN_P_OFFSET".format(arm), itm_G_pit)
        # Alignment dither actuator for the yaw.
        self.y_ad = AlignmentDither(ezca,
            ":SUS-ITM{0}_M0_OPTICALIGN_Y_OFFSET".format(arm), itm_G_yaw)

        # Create a tracker object for the averaged power.
        self.power_gradient = Tracker()
        
        # Create a flag for the first iteration.
        self.first = True
        
        # Create a counter for tracking successful iterations.
        self.counter = 0
        
        # Create a timer for maximum timeout.
        self.timer["timeout"] = 130

        # Set a timer, to allow changes to take effect.
        self.timer["wait"] = 10
        self.timer["move-on"] = 60

        # Filename for writing data to
        #pathname = '/opt/rtcds/userapps/trunk/asc/l1/guardian/data/'
        pathname = "/data/ASC/InitialAlignment/ITM{0}/".format(arm)
        filetime = str(int(gpstime.gpsnow()))
        filename = "itm{0}_to_etm{0}_bpd".format(arm.lower())
        self.fname = pathname + filename + '_' + filetime + '.txt'

    # Run class method - Run the automatic alignment process.
    def run(self):
        # Wait for changes to take effect
        if not self.timer["wait"]:
            # Don't proceed until the timer elapses.
            return False

        # Collect the data, takes the default of 2 seconds.
        PD_pwr, Pit_demod, Yaw_demod = self.dig_demod.demod()
        
        # Save the value for the gradient tracking.
        self.power_gradient.value = PD_pwr

        #ajm20191028 commented out
        # Check that the power is high enough.
        #if PD_pwr < itm_stop.pwr_mn:
        #    # Inform the operator that the power is too low.
        #    log("Green power on ETM{0} Baffle PD 1, too low.".format(arm))
        #    notify("Green power on ETM{0} Baffle PD 1, too low.".format(arm))
        #    
        #    # Jump to the manual alignment/scan state.
        #    return "FIND_ETM_BFFL_PD_WITH_ITM"

        no_grad = False
        # Check for first iteration.
        if self.first:
            # Set the flag to False, so that it won't run again.
            self.first = False
            no_grad = True
            # Log a header for the DATA.
            header = "GPSTIME\tAVG-TRANS\tPIT-ERROR\tYAW-ERROR\tGRAD-POWER\tCOUNTER\n"
            with open (self.fname, "a") as f:
                f.write(header)
        else:
            # First assume that step was successful.
            self.counter += 1
            
            # Check the stop conditions.
            if abs(self.power_gradient.gradient) > itm_stop.grd_ok:
                self.counter = 0
                log("|Gradient of average green power|, too high.")
            
            if abs(Pit_demod) > itm_stop.pit_ok:
                self.counter = 0
                log("|Pitch error|, too large.")
            
            if abs(Yaw_demod) > itm_stop.yaw_ok:
                self.counter = 0
                log("|Yaw error|, too large.")

            if abs(PD_pwr) < itm_stop.pwr_ok:
                self.counter = 0
                log("Baffle PD power too low")

            # Check final stop conditions.
            #if (self.counter >= 3):
            #    # We are done.
            #    return True
            #else:
            #    pass

            # Uncomment this and comment out above 'return True'), if wanting to make it a timed thing
            if self.timer["move-on"]:
                return True

        # ajm20191025 commented out
        # Check for timeout.
        #if self.timer["timeout"]:
        #    # Inform the operator.
        #    log("Timeout on ITM{0} alignment.".format(arm))
        #    notify("Timeout on ITM{0} alignment. ".format(arm) + \
        #           "Please manually check/confirm alignment. " + \
        #           "You may rerun this step, if deemed appropriate.")
        #    
        #    # Jump to the manual/scan state.
        #    return "FIND_ETM_BFFL_PD_WITH_ITM"


        # Actuate the ITM
        self.p_ad.align_dither(Pit_demod, PD_pwr)
        self.y_ad.align_dither(Yaw_demod, PD_pwr)

        if no_grad:
            p_gradient = 0
        else:
            p_gradient = self.power_gradient.gradient

        # write important data to file for analysis
        gtime = gpstime.gpsnow()
        data = "{0:.3f}\t{1:.3e}\t{2:.3e}\t{3:.3e}\t{4:.3e}\t{5}\n".format(gtime,PD_pwr,Pit_demod,Yaw_demod,p_gradient,self.counter)
        with open (self.fname, "a") as f:
            f.write(data)



#------------------------------------------------------------------------------

# ITM Aligned state - Ensures the ITM is aligned to the ETM baffle
class ITM_ALIGNED_TO_ETM(GuardState):
    request = True
    index = 25

    # Main class method - Turn off oscillator and save position.
    def main(self):

        # Turn off ITM test filter offsets.
        # This points the ITM back towards the ETM.
        ezca.switch(":SUS-ITM{0}_M0_TEST_P".format(arm), "OFFSET", "OFF")
        ezca.switch(":SUS-ITM{0}_M0_TEST_Y".format(arm), "OFFSET", "OFF")

        # Disable the TMS pitch and yaw oscillators.
        turn_off_tms_osc()

        # TODO: Assess below code, keep?
        # Write oplev values to the ITM L2 OLDAMP
        #ezca.write(":SUS-ITM{0}_L2_OLDAMP_P_OFFSET".format(arm), -1 * ezca.read(":SUS-ITM{0}_L2_OLDAMP_P_INMON".format(arm)))
        #ezca.write(":SUS-ITM{0}_L2_OLDAMP_Y_OFFSET".format(arm), -1 * ezca.read(":SUS-ITM{0}_L2_OLDAMP_Y_INMON".format(arm)))

        # Save the position of the ITM.
        #align_save(ITM, ezca)

        ##FIXME: Should remove this.
        #self.monitor = RunningAvg(ezca,
        #                          ":AOS-ETM{0}_BAFFLEPD_1_POWER".format(arm),
        #                          1)
        #
        ## Fill up the running average buffer.
        #log("Filling running average buffer.")
        #
        #_, is_done = self.monitor.avg()
        #
        #while not is_done:
        #    _, is_done = self.monitor.avg()
        #
        #log("Running average buffer filled.")

    # Run class method - Ensure that the power on the ETM baffle
    # sufficient.
    def run(self):
        # Collect the data
        #avg, _ = self.monitor.avg()
        
        ## Check the power.
        #if avg < itm_stop.pwr_ok:
        #    # Inform the operators.
        #    log("ETM{0} baffle PD has fallen below {1} to {2}".format(arm, itm_stop.pwr_ok, avg))
        #    notify("ETM{0} baffle PD has fallen below {1} to {2}".format(arm, itm_stop.pwr_ok, avg))
        #    
        #    # Jump to manual alignment.
        #    return "FIND_ETM_BFFL_PD_WITH_ITM"
        ## Power is fine.
        #else:
        #    # Succeed.
        #    return True

        return True

#------------------------------------------------------------------------------
class POINT_ETM_TO_ITM_BFFL_PD(GuardState):
    request = False
    index = 26

    def main(self):

        # Inform the operator.
        log("Pointing ETM{0} to ITM{0} Baffle PD 1.".format(arm))

        #------------------------------------------------------- 
        # Ramp the offsets to pre-saved values over 10 seconds.

        # Set ETM optic align ramp times.
        etm.optic_align_ramp('P', 1, ezca)
        etm.optic_align_ramp('Y', 1, ezca)

        ## Turn the offsets and outputs on.
        etm.misalign('P', etm_bpd1_ofs['P'], ezca)
        etm.misalign('Y', etm_bpd1_ofs['Y'], ezca)

        if not pr2.is_misaligned('P',ezca):
            pr2.misalign('P', -500, ezca)
        #------------------------------------------------------- 

        self.timer['wait'] = 5

        self.bffl_pd = EzAvg(ezca,3,":AOS-ITM{0}_BAFFLEPD_1_POWER".format(arm))

    def run(self):

        if not self.timer['wait']:
            return

        [done,val] = self.bffl_pd.ezAvg()
        if not done:
            return
        elif val > etm_stop.pwr_mn:
            log('Sufficient power in Baffle PD, will begin optimization')
            return 'ALIGN_ETM_TO_ITM_BFFL'
        else:
            log('Baffle PD light too low, will scan for it')
            return 'FIND_ITM_BFFL_PD_WITH_ETM'

        return True

#------------------------------------------------------------------------------
FIND_ITM_BFFL_PD_WITH_ETM = make_bfflpdfinder_state('ETM')
FIND_ITM_BFFL_PD_WITH_ETM.index = 27

#------------------------------------------------------------------------------
# Align ETM to ITM baffle
class ALIGN_ETM_TO_ITM_BFFL(GuardState):
    request = False
    index = 29    
    
    # Main class method - Set up the automatic alignment.
    def main(self):

        # Inform the operator.
        log("Aligning ETM{0} to ITM{0} Baffle PD 1.".format(arm))

        # Misalign PR2, IR light corrupts the BPD signal
        if not pr2.is_misaligned('P',ezca):
            pr2.misalign('P', -500, ezca)

        # Enable the TMS pitch oscillator. #TODO: Question, should we dither ETM here instead?
        turn_on_tms_osc()
        
        #-------------------------------------------------
        # Align ETM to the baffle PD
        etm.misalign('P', etm_bpd1_ofs['P'], ezca)
        etm.misalign('Y', etm_bpd1_ofs['Y'], ezca)
        #-------------------------------------------------

        etm.optic_align_ramp('P', 1, ezca)
        etm.optic_align_ramp('Y', 1, ezca)


        # Digital demodulation, CIC filter the data.
        self.dig_demod = DigitalDemodulation(ezca,
            ":AOS-ITM{0}_BAFFLEPD_1_POWER".format(arm),
            ":SUS-TMS{0}_M1_DITHER_P_INMON".format(arm),
            ":SUS-TMS{0}_M1_DITHER_Y_INMON".format(arm))

        # Alignment dither actuator for the pitch.
        # Missing the offset channel.
        self.p_ad = AlignmentDither(ezca,
            ":SUS-ETM{0}_M0_OPTICALIGN_P_OFFSET".format(arm), etm_G_pit)
        
        # Alignment dither actuator for the yaw.
        # Missing the offset channel.
        self.y_ad = AlignmentDither(ezca,
            ":SUS-ETM{0}_M0_OPTICALIGN_Y_OFFSET".format(arm), etm_G_yaw)

        # Create a tracker object for the averaged power.
        self.power_gradient = Tracker()
        
        # Create a flag for the first iteration.
        self.first = True
        
        # Create a counter for tracking successful iterations.
        self.counter = 0
        
        # Create a timer for maximum timeout.
        self.timer["timeout"] = 130

        # Set a timer, to allow changes to take effect.
        self.timer["wait"] = 10
        self.timer["move-on"] = 80

        # Filename for writing data to
        #pathname = "/opt/rtcds/userapps/trunk/asc/l1/guardian/data/"
        pathname = "/data/ASC/InitialAlignment/ETM{0}/".format(arm)
        filetime = str(int(gpstime.gpsnow()))
        filename = "etm{0}_to_itm{0}_bpd".format(arm.lower())
        self.fname = pathname + filename + '_' + filetime + '.txt'

    # Run class method - Run the automatic alignment process.
    def run(self):
        # Wait for changes to take effect
        if not self.timer["wait"]:
            # Don't proceed until the timer elapses.
            return False

        # Collect the data, takes the default of 2 seconds.
        PD_pwr, Pit_demod, Yaw_demod = self.dig_demod.demod()
        
        # Save the value for the gradient tracking.
        self.power_gradient.value = PD_pwr

        #ajm20191028 commented out
        # Check that the power is high enough.
        #if PD_pwr < itm_stop.pwr_mn:
        #    # Inform the operator that the power is too low.
        #    log("Green power on ETM{0} Baffle PD 1, too low.".format(arm))
        #    notify("Green power on ETM{0} Baffle PD 1, too low.".format(arm))
        #    
        #    # Jump to the manual alignment/scan state.
        #    return "FIND_ETM_BFFL_PD_WITH_ITM"

        no_grad = False
        # Check for first iteration.
        if self.first:
            # Set the flag to False, so that it won't run again.
            self.first = False
            no_grad = True
            # Log a header for the DATA.
            header = "GPSTIME\tAVG-TRANS\tPIT-ERROR\tYAW-ERROR\tGRAD-POWER\tCOUNTER\n"
            with open (self.fname, "a") as f:
                f.write(header)
        else:
            # First assume that step was successful.
            self.counter += 1
            
            # Check the stop conditions.
            if abs(self.power_gradient.gradient) > etm_stop.grd_ok:
                # Reset the counter - unacceptably high power gradient.
                self.counter = 0
                # Log the problem.
                log("|Gradient of average green power|, too high.")

            if abs(Pit_demod) > etm_stop.pit_ok:
                # Reset the counter - unacceptably high pitch error.
                self.counter = 0
                # Log the problem.
                log("|Pitch error|, too large.")
            
            if abs(Yaw_demod) > etm_stop.yaw_ok:
                # Reset the counter - unacceptably high yaw error.
                self.counter = 0
                # Log the problem.
                log("|Yaw error|, too large.")

            if abs(PD_pwr) < etm_stop.pwr_ok:
                # Reset the counter - unacceptably high yaw error.
                self.counter = 0
                # Log the problem.
                log("Baffle PD power too low")

            # Check final stop conditions.
            #if (self.counter >= 3):
            #    # We are done.
            #    return True
            #else:
            #    pass

            # Uncomment this and comment out above 'return True'), if wanting to make it a timed thing
            if self.timer["move-on"]:
                return True

        # ajm20191025 commented out
        # Check for timeout.
        #if self.timer["timeout"]:
        #    # Inform the operator.
        #    log("Timeout on ITM{0} alignment.".format(arm))
        #    notify("Timeout on ITM{0} alignment. ".format(arm) + \
        #           "Please manually check/confirm alignment. " + \
        #           "You may rerun this step, if deemed appropriate.")
        #    
        #    # Jump to the manual/scan state.
        #    return "FIND_ETM_BFFL_PD_WITH_ITM"

        # Actuate the ITM
        self.p_ad.align_dither(Pit_demod, PD_pwr)
        self.y_ad.align_dither(Yaw_demod, PD_pwr)

        if no_grad:
            p_gradient = 0
        else:
            p_gradient = self.power_gradient.gradient

        # write important data to file for analysis
        gtime = gpstime.gpsnow()
        data = "{0:.3f}\t{1:.3e}\t{2:.3e}\t{3:.3e}\t{4:.3e}\t{5}\n".format(gtime,PD_pwr,Pit_demod,Yaw_demod,p_gradient,self.counter)
        with open (self.fname, "a") as f:
            f.write(data)

#------------------------------------------------------------------------------
# ITM Aligned state - Ensures the ITM is aligned to the ETM baffle
class ETM_ALIGNED_TO_ITM(GuardState):
    request = True
    index = 30

    # Main class method - Turn off oscillator and save position.
    def main(self):

        # Turn off ETM test filter offsets.
        # This points the ETM back towards the ETM.
        ezca.switch(":SUS-ETM{0}_M0_TEST_P".format(arm), "OFFSET", "OFF")
        ezca.switch(":SUS-ETM{0}_M0_TEST_Y".format(arm), "OFFSET", "OFF")

        # Disable the TMS pitch and yaw oscillators.
        turn_off_tms_osc()

        # TODO: Assess below code, keep?
        # Write oplev values to the ETM L2 OLDAMP
        #ezca.write(":SUS-ETM{0}_L2_OLDAMP_P_OFFSET".format(arm), -1 * ezca.read(":SUS-ETM{0}_L2_OLDAMP_P_INMON".format(arm)))
        #ezca.write(":SUS-ETM{0}_L2_OLDAMP_Y_OFFSET".format(arm), -1 * ezca.read(":SUS-ETM{0}_L2_OLDAMP_Y_INMON".format(arm)))

        # Save the position of the ETM.
        #align_save(ETM, ezca)

        #self.monitor = RunningAvg(ezca,
        #                          ":AOS-ETM{0}_BAFFLEPD_1_POWER".format(arm),
        #                          1)
        #
        ## Fill up the running average buffer.
        #log("Filling running average buffer.")
        #
        #_, is_done = self.monitor.avg()
        #
        #while not is_done:
        #    _, is_done = self.monitor.avg()
        #
        #log("Running average buffer filled.")

    # Run class method - Ensure that the power on the ETM baffle
    # sufficient.
    def run(self):
        ## Collect the data
        #avg, _ = self.monitor.avg()
        
        ## Check the power.
        #if avg < itm_stop.pwr_ok:
        #    # Inform the operators.
        #    log("ETM{0} baffle PD has fallen below {1} to {2}".format(arm, itm_stop.pwr_ok, avg))
        #    notify("ETM{0} baffle PD has fallen below {1} to {2}".format(arm, itm_stop.pwr_ok, avg))
        #    
        #    # Jump to manual alignment.
        #    return "FIND_ETM_BFFL_PD_WITH_ITM"
        ## Power is fine.
        #else:
        #    # Succeed.
        #    return True

        return True

#------------------------------------------------------------------------------
# Green Cavity PDH Error State.
class GRN_ARM_LOCK_ERROR(GuardState):
    request = False
    index = 36

    def main(self):
        self.als_state = "PDH_LOCKED"

    def run(self):    
        notify("ALS_{0}ARM guardian failed to reach ".format(arm) + \
               "\"PDH_LOCKED\" within 60 seconds. Human intervention required.")

        if als_loose.state != self.als_state or not als_loose.completed:
            return False

        return True

#------------------------------------------------------------------------------

class POINT_MIRRORS_FOR_GRN_LOCKING(GuardState):
    request = False
    index = 37
    
    def main(self):
        # Ensure ITMs and ETMs are pointing towards the ends 
        # I.e. this turns off the test filter offsets.
        #for dof in ['P','Y']:
        #    itm.align(dof,ezca)
        #    etm.align(dof,ezca)
            
        return True

#------------------------------------------------------------------------------

#FIXME: moved to ALS guardian, delete when satisfied
'''
class SCAN_AND_FIND_RESONANCES(GuardState):
    request = False
    index = 38

    def main(self):

        #als_loose.set_request('PDH_LOCKED')
        als_loose.set_request('RELOCK_PDH')

        # Set test gains to match optic align gains
        tms.copy_oagain_to_test("P", ezca)
        tms.copy_oagain_to_test("Y", ezca)
        etm.copy_oagain_to_test("P", ezca)
        etm.copy_oagain_to_test("Y", ezca)

        # Channels
        self.chan = {}
        for optic in ["ETM","TMS"]:
            self.chan[optic] = {}
            if optic=="ETM":
                stage = "M0"
            else:
                stage = "M1"
            for dof in ["P","Y"]:
                self.chan[optic][dof] = 'SUS-'+optic+arm+'_'+stage+'_TEST_'+dof+'_'

        self.transPdChan = 'ALS-C_TR'+arm+'_A_LF_OUTPUT'

        etmAmpl = 1.5 #1.0    #max amplitude (amplitude of triangle wave)
        etmFreq1 = 0.2 #0.4		#frequency of sine waves
        etmFreq2 = 0.008 #0.016	#frequency of envelope (triangle wave)
        tmsAmpl = 1.5 #1.0	#max amplitude (amplitude of triangle wave)
        tmsFreq1 = 0.1 #0.05		#frequency of sine waves
        tmsFreq2 = 0.002 #0.004	#frequency of envelope (triangle wave)
        f_s = 16384		#sampling frequency
        duration = 125

        log("Generating waveforms")
        etmYaw,etmPit = spiral_waveforms(etmAmpl,etmFreq1,etmFreq2,f_s,duration)
        tmsYaw,tmsPit = spiral_waveforms(tmsAmpl,tmsFreq1,tmsFreq2,f_s,duration)

        #Tuple the waveforms
        self.wforms = (etmPit,etmYaw,tmsPit,tmsYaw)

        self.transPD = np.array([])
        self.etmPitTestOut = np.array([])
        self.etmYawTestOut = np.array([])
        self.tmsPitTestOut = np.array([])
        self.tmsYawTestOut = np.array([])

        #Set-up scan
        tnow = int(gpstime.tconvert('now'))
        self.tstart = tnow + 10
        self.tnext = self.tstart-1
        log("Start time is {}".format(int(self.tstart)))
        log("Opening awg streams")

        self.exc = []
        for optic in ["ETM","TMS"]:
            for dof in ["P","Y"]:
                self.exc.append(awg.ArbitraryStream( IFO+":"+self.chan[optic][dof]+"EXC",rate=f_s, start=self.tstart , appinfo="two stream"))
        for ex in self.exc:
            ex.open()

        self.ii = 0

        self.tappend = 1.0
        #self.step = self.tappend*f_s #FIXME: this needs to be the number of steps (=fs)
        self.step = f_s
        self.append_stream = True
        self.scan_started = False
        self.savedata = True
        self.monitor = True
        self.offload_alignment = False

    def run(self):

        if self.append_stream:
            dt = self.tnext - self.tstart
            log("Time from t0: %d" % dt )
            end = min(self.ii+self.step, len(self.wforms[0]))
            #Append next part of waveforms to streams
            for exnum in range(len(self.exc)):
                self.exc[exnum].append(self.wforms[exnum][self.ii:end])

            self.ii += self.step
            self.append_stream = False

        if (self.monitor and int(gpstime.tconvert('now')) < self.tnext):
            if not self.scan_started: 
                if gpstime.tconvert('now')>(self.tstart-0.2):
                    log("Scanning and recording data, time is {0}".format(gpstime.tconvert('now')))
                    self.scan_started = True
                time.sleep(0.2)
                return
            elif ezca[self.transPdChan] > 1100:
                log('TMS above threshold, hold')
                ezca.switch('SUS-ETM{}_M0_TEST_P'.format(arm),'HOLD','ON')
                ezca.switch('SUS-ETM{}_M0_TEST_Y'.format(arm),'HOLD','ON')
                ezca.switch('SUS-TMS{}_M1_TEST_P'.format(arm),'HOLD','ON')
                ezca.switch('SUS-TMS{}_M1_TEST_Y'.format(arm),'HOLD','ON')
                for exnum in range(len(self.exc)):
                    self.exc[exnum].abort()
                    self.exc[exnum].close()
                self.monitor = False
                self.offload_alignment = True
                return
            else:
                self.transPD = np.append(self.transPD, ezca[self.transPdChan])
                self.etmPitTestOut = np.append(self.etmPitTestOut, ezca[self.chan["ETM"]["P"]+'OUTPUT'])
                self.etmYawTestOut = np.append(self.etmYawTestOut, ezca[self.chan["ETM"]["Y"]+'OUTPUT'])
                self.tmsPitTestOut = np.append(self.tmsPitTestOut, ezca[self.chan["TMS"]["P"]+'OUTPUT'])
                self.tmsYawTestOut = np.append(self.tmsYawTestOut, ezca[self.chan["TMS"]["Y"]+'OUTPUT'])
                return
        elif (self.monitor and self.ii<len(self.wforms[0])):
            self.tnext += self.tappend
            self.append_stream = True
            return

        log("Finished scan")

        if self.offload_alignment:
            log('Offloading TEST outputs to OPTICALIGN offsets')
            offload_test_alignment('TMS','P',tramp=3.0)
            offload_test_alignment('TMS','Y',tramp=3.0)
            offload_test_alignment('ETM','P',tramp=3.0)
            offload_test_alignment('ETM','Y',tramp=3.0)
            als_loose.set_request('PDH_LOCKED')
            time.sleep(3)
            self.offload_alignment=False


        if self.savedata:
            filedir = '/data/ASC/InitialAlignment/'+arm+'ARM/'
            filename = filedir + 'etm{0}_tms{0}_ScanForResonance_{1}.npz'.format(arm.lower(),int(self.tstart))
            log("Saving data to {}".format(filename))
            np.savez(filename, transPD=self.transPD, etmPitTestOut=self.etmPitTestOut, etmYawTestOut=self.etmYawTestOut, tmsPitTestOut=self.tmsPitTestOut, tmsYawTestOut=self.tmsYawTestOut)
            self.savedata = False




#        if self.find_max:
#            thresh = {'X':,'Y':}
#            peaks = peak_finder(self.transPD,thresh[arm])
#            if len(peaks[0])==0:
#                return GRN_ARM_LOCK_ERROR
#            v_max = np.max(peaks[0])
#            i_max = peaks[1,np.argmax(peaks[0])]

        return True
'''
#------------------------------------------------------------------------------
# Green Cavity Lock Acquition state - does what it says.
class LOCK_GRN_ARM(GuardState):
    request = False
    index = 39
    
    # Main class method - Request that the ALS locks the green cavity.
    def main(self):

        # Release the als guardian, to do its own thing.
        als_loose.release()

        # Send instructions to managed ALS guardian.
        self.als_state = "PDH_LOCKED"
        als_loose.set_request(self.als_state)

        # Set a check timer.
        self.timer["wait"] = 2
        
        # Set a maximum timeout timer.
        self.timer["timeout"] = 120

    # Run class method - Periodically check the ALS guardian has
    #locked, and manage any errors it throws.
    def run(self):
        # The timer, to check, has expired.
        if self.timer["wait"]:
            # The ALS is PDH locking the green arms. TODO: Should also make sure state completes.
            if als_loose.state == self.als_state and als_loose.done:
                # Succeed.
                return True
            # The ALS is not yet PDH locking the green arms.
            else:
                # Reset the timer.
                self.timer["wait"] = 2

        # The timout timer has expired.
        if self.timer["timeout"]:
            # Inform the operator of the error.
            log("ALS_{0}ARM guardian failed to reach ".format(arm) + \
                "\"PDH_LOCKED\" within 60 seconds (timeout).")
            notify("ALS_{0}ARM guardian failed to reach ".format(arm) + \
                   "\"PDH_LOCKED\" within 60 seconds (timeout).")

            # Move to the error state.
            return "GRN_ARM_LOCK_ERROR"

#------------------------------------------------------------------------------
# Green Cavity PDH Lock state - does what it says.
class GRN_ARM_LOCKED(GuardState):
    request = True
    index = 40

    def main(self):
        self.als_state = "PDH_LOCKED"

    # Run class method - Ensure that the ALS guardian has finished PDH
    # locking the green arm cavity.
    def run(self):
        # Check to see if the ALS is incompleted.
        #if not als_loose.completed:
        #    # Fail to proceed.
        #    return False
        # Check for other management failures.
        # TODO: More sophisticated checks.
        if als_loose.state != self.als_state or not als_loose.completed:
            # Inform the operator that there is an error.
            log("ALS_{0}ARM guardian is not in \"PDH_LOCKED\".".format(arm))
            notify("ALS_{0}ARM guardian is not in \"PDH_LOCKED\".".format(arm))

            # Move to the error state.
            return "LOCK_GRN_ARM"
        else:
            # Succeed.
            return True

#------------------------------------------------------------------------------
'''
# Dither Align TMS while moving ITM and ETM to set the cavity axis.
#TODO: Ensure TMS dither frequenices are different for X and Y 
class ALIGN_CAVITY_AXIS(GuardState):
    request = False
    index = 43

    def main(self):

        self.als_state = "PDH_LOCKED"

        #--------------------------------------------
        # Turn on demod and instantiate the digital demodulation and feedback
        if arm == 'Y':
            turn_on_tms_osc(ampl=100)
        else:
            turn_on_tms_osc(ampl=200)
        
        tms.optic_align_ramp('P', 0.1, ezca)
        tms.optic_align_ramp('Y', 0.1, ezca)

        # Digital demodulation of the arm trans with the TMS dither.
        self.dig_demod = DigitalDemodulation(ezca,
            ":ALS-C_TR{0}_A_LF_OUTPUT".format(arm),
            ":SUS-TMS{0}_M1_LOCK_P_INMON".format(arm),
            ":SUS-TMS{0}_M1_LOCK_Y_INMON".format(arm))

        # Alignment dither actuator for the pitch.
        self.p_ad = AlignmentDither(ezca,
            ":SUS-TMS{0}_M1_OPTICALIGN_P_OFFSET".format(arm), arm_G_pit)
        
        # Alignment dither actuator for the yaw.
        self.y_ad = AlignmentDither(ezca,
            ":SUS-TMS{0}_M1_OPTICALIGN_Y_OFFSET".format(arm), arm_G_yaw)

        #--------------------------------------------
        # Set-up for centering the arm spots
        self.spot_center = CentroidCentering(ezca,arm,cam_spots_nom,
                                         pitDmtrx = drive_mtrx['P'],
                                         yawDmtrx = drive_mtrx['Y'],
                                         gain = 0.25)

        #--------------------------------------------

        self.counter = 0

        # Create a flag for the first iteration.
        self.first = True
        
        self.timer["settle"] = 0
        self.timer["move-on"] = 100

        self.center_mode = True
        self.feedback_mode = True

        # Write data to file?
        self.write_to_file = False
        if self.write_to_file:
            # Filename for writing data to
            #pathname = "/opt/rtcds/userapps/trunk/asc/l1/guardian/data/"
            pathname = "/data/ASC/InitialAlignment/{0}ARM/".format(arm)
            filetime = str(int(gpstime.gpsnow()))
            filename = "tms{0}_to_{0}arm_cavity".format(arm.lower())
            self.fname = pathname + filename + '_' + filetime + '.txt'

    # TODO Make sure green trans stays above some threshold, if not jump back to locking.
    # Alternatively, just check that the ALS guardian is in PDH locked state. 
    def run(self):
        
        #TODO:ensure als is managed?        
        if als_loose.state != self.als_state:
            # Inform the operator that there is an error.
            log("ALS_{0}ARM guardian is not in \"PDH_LOCKED\".".format(arm))
            notify("ALS_{0}ARM guardian is not in \"PDH_LOCKED\".".format(arm))

            return "LOCK_GRN_ARM"

        # Get demod error signals, if they are greater than the acceptable level for centering spots, continue driving
        # Collect the data, takes the default of 2 seconds.
        PD_pwr, Pit_demod, Yaw_demod = self.dig_demod.demod()

        no_grad = False
        # Check for first iteration.
        if self.first:
            # Set the flag to False, so that it won't run again.
            self.first = False
            no_grad = True
            if self.write_to_file:
                # Log a header for the DATA.
                header = "GPSTIME\tAVG-TRANS\tPIT-ERROR\tYAW-ERROR\n"
                with open (self.fname, "a") as f:
                    f.write(header)
                return

        log("Pit error is {}".format(Pit_demod))
        log("Yaw error is {}".format(Yaw_demod))

        # write important data to file for analysis
        if self.write_to_file:
            gtime = gpstime.gpsnow()
            data = "{0:.3f}\t{1:.3e}\t{2:.3e}\t{3:.3e}\n".format(gtime,PD_pwr,Pit_demod,Yaw_demod)
            with open (self.fname, "a") as f:
                f.write(data)


        if self.center_mode:
             self.pit_ok = arm_stop.pit_ok
             self.yaw_ok = arm_stop.yaw_ok
             self.pwr_ok = arm_stop.pwr_min
        else:
             self.pit_ok = arm_stop.pit_aligned
             self.yaw_ok = arm_stop.yaw_aligned
             self.pwr_ok = arm_stop.pwr_aligned


        # In centering mode
        self.counter += 1

      
        if abs(Pit_demod) > self.pit_ok:
            self.counter = 0
            #log("|Pitch error|, too large.")

        if abs(Yaw_demod) > self.yaw_ok:
            self.counter = 0
            #log("|Yaw error|, too large.")

        if abs(PD_pwr) < self.pwr_ok:
            self.counter = 0
            #log("Green Trans Power Too Low")
                
            # Check final stop conditions.
            #if (self.counter >= 3):
            #    # We are done.
            #    return True


        log("counter: {}".format(self.counter))

        # If the TMS demod error is sufficiently low, center the spots
        if self.center_mode and self.counter>=5:
            log("Checking spot positions")
            spot_diffs = self.spot_center.diff_spots(['P','Y'])
            log("Pitch spot diffs: {0}".format(spot_diffs['P']))
            log("Yaw spot diffs: {0}".format(spot_diffs['Y']))
                 
            if any(abs(diff)>1.0 for diff in spot_diffs['P']+spot_diffs['Y']):
                log("Spots not yet centered, moving TMs")
                self.spot_center.center_arm_spots(['P','Y'])
                self.counter = 0
                self.timer["settle"] = 5
                return
            else:
                log("Spots centered well enough")
                self.center_mode = False
                self.counter = 0
                self.timer['move-on'] = 10
                return

        if (not self.center_mode) and self.counter>=3 and self.timer['move-on']:
             self.feedback_mode = False

        if not self.timer["settle"]:
            return

        if self.feedback_mode:
             self.p_ad.align_dither(Pit_demod, PD_pwr)
             self.y_ad.align_dither(Yaw_demod, PD_pwr)
             return


        return True
'''



#------------------------------------------------------------------------------

class ALIGN_CAVITY_AXIS(GuardState):
    request = False
    index = 42

    def main(self):

        # Written as the matrix shows, row is actuator, col is camera
        drive_mtrx = {'PIT':{'ITM':{'I':-1.07,'E':-1.0},
                             'ETM':{'I':-1.0,'E':-0.78}},

                      'YAW':{'ITM':{'I':-1.07,'E':1.0},
                             'ETM':{'I':1.0,'E':-0.78}}
                     }


        # Set the output matrices
        for act in ['ITM','ETM']:
            for cam in ['I','E']:
                matrix.asc_ads_output_pit[act+arm,'PIT{}'.format(cam_dof_num[cam])] = drive_mtrx['PIT'][act][cam]
                matrix.asc_ads_output_yaw[act+arm,'YAW{}'.format(cam_dof_num[cam])] = drive_mtrx['YAW'][act][cam]

        self.als_state = 'ALIGN_ARM'
        als_loose.set_request(self.als_state)

        errChans = []
        self.thresholds = []
        for dof in ['PIT','YAW']:
            errChans.append(':ASC-ADS_{}{}_DOF_OUTPUT'.format(dof,tms_osc_number))
            self.thresholds.append(0.05)
            for cam in ['I','E']:
                errChans.append(':ASC-ADS_{}{}_DOF_OUTPUT'.format(dof,cam_dof_num[cam]))
                self.thresholds.append(150)

        self.errSigAvg = EzAvg(ezca,30,errChans)

    def run(self):

        if als_loose.state != self.als_state or not als_loose.completed or not als_loose.done:
            notify('Waiting for ALS guardian')
            #TODO: reset ezavg buffer
            return

        #TODO: wait for error signals to be small, once small enough, return ALS to PDH LOCKED, 
        #integrators should hold alignment

        [done,vals] = self.errSigAvg.ezAvg()
        if not done:
            notify('Waiting for esigs avg buffer to fill')
            return

        if any(abs(vals[ii])>self.thresholds[ii] for ii in range(6)):
            notify('Waiting for esigs to zero')
            return

        return True


#------------------------------------------------------------------------------

class OFFLOAD_SUSPENSIONS(GuardState):
    request = False
    index = 43

    def main(self):

        # ALS guardian offloads alignment when going from ALIGN_ARM to PDH_LOCKED
        self.als_state = 'PDH_LOCKED'
        als_loose.set_request(self.als_state)

    def run(self):

        if als_loose.state != self.als_state or not als_loose.completed or not als_loose.done:
            notify('Wait for als guardian to return to PDH_LOCKED')
            return

        return True



#------------------------------------------------------------------------------
# Dither Align TMS and ETM using transmitted green beam 
#TODO: Ensure TMS and ETM dither frequenices are different for X and Y 
class GRN_CAVITY_ALIGNED(GuardState):
    request = True
    index = 45

    def main(self):

        turn_off_tms_osc()

        align_save(TMS, ezca)
        align_save(ITM, ezca)
        align_save(ETM, ezca)

    def run(self):

        notify("Arm Cavity Aligned with Green")

        return True


#------------------------------------------------------------------------------
# Manual Green Cavity alignment state - 
class ALIGN_GRN_MANUAL(GuardState):
    request = True
    index = 41

    # Main class method - Releases the ALS guardian.
    def main(self):
        # Create a counter for successful steps.
        self.success_counter = 0

        # Reset the ETM OPTICALIGN_TRAMPs to 1 second.
        etm.optic_align_ramp('P', 1, ezca)
        etm.optic_align_ramp('Y', 1, ezca)
        
        # Reset the TMS OTICALIGN_TRAMPs, to 1 second.
        tms.optic_align_ramp('P', 1, ezca)
        tms.optic_align_ramp('Y', 1, ezca)

        # TODO: Any additional setup for monitoring/controlling this
        # step.

        # Inform the operator.
        log("Please manually align ETM{0}.".format(arm) + \
            " Some fine adjustment to TMS{0} is also required.".format(arm))
        notify("Please manually align ETM{0}.".format(arm) + \
               " Some fine adjustment to TMS{0} is also required.".format(arm))
        
        # Create a remind timer, 15 seconds.
        self.timer["remind"] = 15

    # Run class method - Monitor progress of the alignment.
    # TODO: In future do this automatically (for example with the image
    # analysis code, or a triggered servo).
    def run(self):
        # Check for reminder timer expiration.
        if self.timer["remind"]:
            # Remind the operator.
            log("Please manually align ETM{0}. Some".format(arm) + \
                " fine adjustment to TMS{0} is also required.".format(arm))
            notify("Please manually align ETM{0}. Some fine".format(arm) + \
                   " adjustment to TMS{0} is also required. ".format(arm) + \
                   "Target is L1:ALS-C_TR{0}_A_LF_OUTPUT".format(arm) + \
                   " > {0}".format(arm_stop.pwr_ok))
            
            # Reset the reminder timer.
            self.timer["remind"] = 15

        # Check the ALS state.
        if als_loose.state != "PDH_LOCKED":
            # The ALS is not in the correct state, reset the counter.
            self.success_counter = 0
            
            # Fail.
            return False
        # Check that the ALS has completed the PDH locked state.
        elif not als_loose.completed:
            # The ALS has not completed the correct state, reset
            # counter.
            self.success_counter = 0
            
            # Fail.
            return False

        # TODO: Add automatic ETM alignment here.
      
        # TODO: Implement more sophisticated stop condition.

        # Check if the transmitted power level is high enough.
        if ezca.read("ALS-C_TR{0}_A_LF_OUTPUT".format(arm)) > arm_stop.pwr_ok:
            # Accumulate successes.
            self.success_counter += 1
        # If it isn't then  we have to reset.
        else:
            # Reset the counter.
            self.success_counter = 0

        # Check that we have succeeded, consecutively for more than
        # 5 seconds.
        if self.success_counter > (5 * 16):
            # Inform the operator
            log("{0} Arm successfully aligned!".format(arm))
            notify("{0} Arm successfully aligned! Save ETM and ITM values".format(arm))

            #FIXME: bad to put this in run loop, need a better 
            # Save the good position of the ETM
            #align_save(ETM, ezca)
            
            # Save the good position of the TMS
            #align_save(TMS, ezca)

            # Succeed
            return True

#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
#                                    Edges
#------------------------------------------------------------------------------

# The edges defining the allowed transitions.
edges = [
         # Recovery or initialisation.
         ("INIT", "ARM_ALIGN_IDLE"),
         ("ARM_ALIGN_RESET", "ARM_ALIGN_IDLE"),
         #("ARM_ALIGN_IDLE", "ARM_ALIGN_RESET"),
         #("ARM_ALIGN_RESET", "MISALIGN_FOR_GRN_CAV"),
         ("ARM_ALIGN_IDLE", "ALIGN_GRN_TO_TMS"),
         # TMS alignment.
         #("MISALIGN_FOR_GRN_CAV", "ALIGN_GRN_TO_TMS"),
         ("GRN_NOT_LOCKED_TO_TMS", "ALIGN_GRN_TO_TMS"),
         ("ALIGN_GRN_TO_TMS", "GRN_ALIGNED_TO_TMS"),
         #("GRN_ALIGNED_TO_TMS", "ALIGN_TMS_TO_ITM_BFFL"),
         ("GRN_ALIGNED_TO_TMS", "MISALIGN_FOR_GRN_CAV"),
         # Arm cavity alignment.
         # TMS
         ("MISALIGN_FOR_GRN_CAV", "POINT_TMS_TO_ITM_BFFL_PD"),
         ("POINT_TMS_TO_ITM_BFFL_PD", "ALIGN_TMS_TO_ITM_BFFL"),
         ("POINT_TMS_TO_ITM_BFFL_PD", "FIND_ITM_BFFL_PD_WITH_TMS",2),
         ("FIND_ITM_BFFL_PD_WITH_TMS", "ALIGN_TMS_TO_ITM_BFFL"),
         ("ALIGN_TMS_TO_ITM_BFFL", "TMS_ALIGNED_TO_ITM"),
         # ITM
         ("TMS_ALIGNED_TO_ITM", "POINT_ITM_TO_ETM_BFFL_PD"),
         ("POINT_ITM_TO_ETM_BFFL_PD", "ALIGN_ITM_TO_ETM_BFFL"),
         ("POINT_ITM_TO_ETM_BFFL_PD", "FIND_ETM_BFFL_PD_WITH_ITM",2),
         ("FIND_ETM_BFFL_PD_WITH_ITM", "ALIGN_ITM_TO_ETM_BFFL"),
         ("ALIGN_ITM_TO_ETM_BFFL", "ITM_ALIGNED_TO_ETM"),
         # ETM
         ("ITM_ALIGNED_TO_ETM", "POINT_ETM_TO_ITM_BFFL_PD"),
         ("POINT_ETM_TO_ITM_BFFL_PD","ALIGN_ETM_TO_ITM_BFFL"),
         ("POINT_ETM_TO_ITM_BFFL_PD","FIND_ITM_BFFL_PD_WITH_ETM",2),
         ("FIND_ITM_BFFL_PD_WITH_ETM","ALIGN_ETM_TO_ITM_BFFL"),
         ("ALIGN_ETM_TO_ITM_BFFL", "ETM_ALIGNED_TO_ITM"),
         #
         ("ETM_ALIGNED_TO_ITM", "POINT_MIRRORS_FOR_GRN_LOCKING"),
         ("GRN_ALIGNED_TO_TMS", "POINT_MIRRORS_FOR_GRN_LOCKING"),
         ("POINT_MIRRORS_FOR_GRN_LOCKING", "LOCK_GRN_ARM"),
         ("GRN_ARM_LOCK_ERROR", "LOCK_GRN_ARM"),
         ("LOCK_GRN_ARM", "GRN_ARM_LOCKED"),
         ("GRN_ARM_LOCKED", "ALIGN_CAVITY_AXIS"),
         ("ALIGN_CAVITY_AXIS", "OFFLOAD_SUSPENSIONS"),
         ("OFFLOAD_SUSPENSIONS", "GRN_CAVITY_ALIGNED"),
         #("ALIGN_CAVITY_AXIS", "GRN_CAVITY_ALIGNED"),
         # Manual.
         ("GRN_ARM_LOCKED", "ALIGN_GRN_MANUAL"),
         # Completed.
         #("ARM_ALIGN_RESET", "FINISHED")
        ]

# END.
