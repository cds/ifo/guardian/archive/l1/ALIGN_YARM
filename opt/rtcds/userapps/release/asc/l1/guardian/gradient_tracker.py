# Docstring:
"""
Contains a class to track the gradient of a number, which is fed to the
instance of this class. This class in named Tracker.


Author: Nathan Holland
Contact: nathan.holland@ligo.org
Date: 2019-06-17

Modified: 2019-06-17 (Created).
"""


#------------------------------------------------------------------------------
# Imports:

# Time stamping is necessary for accurately deterimining the gradient.
import time


#------------------------------------------------------------------------------
# Class Definition:
class Tracker(object):
    """
    A class for tracking the gradient of a number, with respect to time. Time
    tracking is automatic. The derivative returned is the backwards, causal,
    derivative.
    
    It is intended to be used with Guardian code.
    """
    
    def __init__(self):
        """
        Instantiates an instance of the Tracker class.
        
        Usage:
        obj = Tracker()
        obj - The, new, instance of the Tracker class.
        """
        # Set the current time
        self._time = None
        
        # Set the current value.
        self._value = None
        
        
        # Set the current dt.
        self._delta_time = None
        
        # Set the current delta value.
        self._delta_value = None
    
    
    @property
    def value(self):
        """
        Get, or set, the current value - whose gradient is to be tracked. When
        setting a new value it is automatically time stamped, and the changes
        are automatically updated.
        """
        # Getter returns the current value.
        return self._value
    
    @value.setter
    def value(self, new_value):
        # Record the current time.
        new_time = time.time()
        
        
        # self._value has not been set yet.
        if self._value is None:
            # Do not set delta_time or delta_value.
            pass
        else:
            # Do set delta_time, and delta_value.
            self._delta_time = new_time - self._time
            self._delta_value = new_value - self._value
        
        
        # Assign the new time, and new value.
        self._time = new_time
        self._value = new_value
    
    
    @property
    def delta_time(self):
        """
        Get the time difference between the time that \'value\' was last set,
        and the time that \'value\' was set prior to that.
        It will fail, return None, if there was no prior, or is no current
        value.
        
        This is a read only property.
        """
        # Getter returns the current value.
        return self._delta_time
    
    
    @property
    def delta_value(self):
        """
        Get the difference between \'value\', and the prior \'value\'. It will
        fail, return None, if there was no prior, or is no current value.
        
        This is a read only property.
        """
        # Getter returns the current value.
        return self._delta_value
    
    
    @property
    def gradient(self):
        """
        Gets the gradient, with respect to time, of the value. This is
        calculated as delta_value / delta_time. Will fail, return None, if
        there was no prior, or is no current value.
        
        This is a read only property.
        """
        # Getter returns the value, as calculated. #FIXME: underscore at start or no underscore?
        return self.delta_value / self.delta_time


# END.
