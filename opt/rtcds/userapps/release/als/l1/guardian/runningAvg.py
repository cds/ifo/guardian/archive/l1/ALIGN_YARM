import time

EPICS_RATE = 0.0625 #1/16.0

class RunningAvg(object):

    def __init__(self, ezca, channels, duration):
        self._ezca = ezca
        self._done = False
        if isinstance(channels, str):
            self._channels = [channels]
        else:
            self._channels = channels
        self._duration = duration
        self._bufferSize = int(duration*16)
        self._circBuffer = []
        for chan in self._channels:
            self._circBuffer.append([0]*self._bufferSize)
        self._bufferPointer = 0
        self._lastTime = time.time()

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return "%s(%s, '%s', '%s)" % (self.__class__.__name__, self._ezca, self._channels, self._duration)

    def avg(self):
        if self._bufferPointer >= self._bufferSize:
            self._bufferPointer = 0
            self._done = True
        if time.time() - self._lastTime >= EPICS_RATE:
            for i, chan in enumerate(self._channels):
                self._circBuffer[i][self._bufferPointer] = float(self._ezca[chan])
            self._lastTime = time.time()
            self._bufferPointer = self._bufferPointer + 1
        return_array = []
        for i, chan in enumerate(self._channels):
            return_array.append(sum(self._circBuffer[i])/float(self._bufferSize))
        if len(return_array) == 1:
            return_array = return_array[0]
        return return_array, self._done

